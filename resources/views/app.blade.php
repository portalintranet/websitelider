<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
  <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
  <link rel="manifest" href="/img/site.webmanifest">
  <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
  <title>{{ $metaTag->title }}</title>
  <meta name="title" content="{{$metaTag->title}}">
  <meta name="keywords" content="{{$metaTag->tags}}">
  <meta name="description" content="{{ $metaTag->description}}">
  <meta name="msapplication-TileColor" content="#da532c">

  <meta name="dc.language" content="pt-BR" />
  <meta name="coverage" content="Worldwide" />
  <meta name="geo.region" content="BR-PR" />
  <meta name="robots" content="index, all" />
  <meta name="robots" content="index, follow" />
  <meta name="Googlebot" content="index, follow" />
  <meta name="expires" content="never" />
  <meta name="distribution" content="Global" />
  <meta name="category" content="{{ $metaTag->category}}" />
  <meta name="service" content="{{ $metaTag->service}}" />
  <meta name="country" content="Brazil" />

 




  <meta name="theme-color" content="#ffffff">
  <meta name="google-site-verification" content="2HnZjubnUj_vw9jLqegQvTue09B5eziokwpCszcwQMo" />
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-C4DLFV09P8"></script>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TN944NV4');
  </script>
  <!-- End Google Tag Manager -->
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-C4DLFV09P8');
  </script>
  @inertiaHead

  @vite('resources/js/app.js')
</head>

<body>
  @inertia
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN944NV4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
</body>


</html>