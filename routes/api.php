<template>
  <v-layout>
    <app-bar-vue></app-bar-vue>

    <v-main>
      <v-container>
        <v-card flat>
          <v-card-title class="text-uppercase">Bem vindo ao nosso portal de novidades</v-card-title>
        </v-card>
      </v-container>

      <v-container>
        <v-row>
          <v-col v-for="item in items" :key="item.id" cols="12" md="4" lg="4" sm="4">
            <v-img cover :src="'/img/blog/' + item.image" max-height="350"></v-img>
            <v-subheader>{{ item.title }}</v-subheader>
            <div v-text="item.body"></div>
          </v-col>
        </v-row>
        <v-divider class="my-4"></v-divider>
      </v-container>

      <footer-vue></footer-vue>
    </v-main>
  </v-layout>
</template>

<script>
import AppBarVue from "../Components/AppBar.vue";
import BreadCrumbVue from "../Components/BreadCrumb.vue";
import FooterVue from "../Components/Footer.vue";

export default {
  props: {
    items: Array,
    title: String,
  },
  components: {
    AppBarVue,
    BreadCrumbVue,
    FooterVue
  },
  data: () => ({
    currentItem: "tab-Web",
    items: ["Web", "Shopping", "Videos", "Images"],
    more: ["News", "Maps", "Books", "Flights", "Apps"],
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  }),

  methods: {
    addItem(item) { },
  },
};
</script>
