<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\ClickController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductGroupController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ShowLiderController;
use App\Http\Controllers\StoresController;
use Illuminate\Support\Facades\Route;
/*
|-------------------------------------------------------------------------+
| Web Routes                                                              |
|-------------------------------------------------------------------------+
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/
Route::get('/',                      [DashboardController::class    , 'index']);
Route::get('/produto/show/{id}',     [ProductController::class , 'show']);
Route::get('/produto/grupo',         [ProductGroupController::class , 'index']);
Route::get('/produto/search/list',   [ProductGroupController::class , 'listProducts']);
Route::get('/produto/grupo/{group}', [ProductGroupController::class , 'show']);
Route::get('/contato',               [ContactController::class      , 'index']);
Route::get('/nossas-lojas',          [StoresController::class       , 'index']);
Route::get('/quem-somos',            [ContactController::class      , 'aboutUs']);
Route::get('/blog',                  [BlogController::class         , 'index']);
Route::post('/contact/send-message', [ContactController::class      , 'sendMessage']);
Route::get('/show-lider',            [ShowLiderController::class     , 'index']);
Route::get('/showlider',             [ShowLiderController::class     , 'index']);
Route::post('/show-lider',           [ShowLiderController::class     , 'store']);
Route::get('/search',                [SearchController::class        , 'index']);
Route::get('/ProductDescription',    [SearchController::class        , 'index']);
Route::post('click/{type?}'         ,[ClickController::class        , 'click']);
Route::get('click/{type?}'          ,[ClickController::class        , 'click']);

