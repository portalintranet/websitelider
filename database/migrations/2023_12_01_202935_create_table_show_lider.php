<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_lider', function (Blueprint $table) {
            $table->id();
            $table->string('name',60);
            $table->string('cpf',20);
            $table->string('cnpj',20);
            $table->string('document',9);
            $table->string('tenantid',6);
            $table->char('d_e_l',1)->default(' ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_show_lider');
    }
};
