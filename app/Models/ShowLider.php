<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowLider extends Model
{
    use HasFactory;
    protected $table = 'show_lider';
    
}
