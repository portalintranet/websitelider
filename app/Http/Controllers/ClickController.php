<?php

namespace App\Http\Controllers;

use App\Models\WhatsappClick;
use Illuminate\Http\Request;

class ClickController extends Controller
{
    public function click($type=false){
        $click = new WhatsappClick();
        //$click->date = now();
        $click->click = 'S';
        $click->save();        
    }
}
