<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use App\Models\Product;
use App\Models\ProductGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $dados = (object) $request->all();

        $items = array();
        $response = DB::table('product')
            ->where('desc', 'like', '%' . $dados->query . '%')
            ->get();
        if (!is_null($response)) {
            $items[0]['type'] = 'subheader';
            $items[0]['title'] = 'Resultado da Busca';
            $nKey = 0;
            foreach ($response as $key => $value) {
                $nKey++;
                $items[$nKey]['title'] = $value->desc;
                $items[$nKey]['prependAvatar'] = '/img/product/' . $value->group . '/' . $value->image;
                $items[$nKey]['subtitle'] = $value->desc;
                $nKey++;
                $items[$nKey]['type'] = 'divider';
                $items[$nKey]['inset'] = true;
            }
        }
        $metaTag = MetaTag::where('page','search')->first();
        return Inertia::render('Search', [
            'items' => $items,
            'term' => $dados->query
        ])->withViewData(compact('metaTag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function listProducts(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
