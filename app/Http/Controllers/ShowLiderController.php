<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestShowLider;
use App\Models\MetaTag;
use App\Models\ShowLider;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ShowLiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metaTag = MetaTag::where('page','home')->first();      
            
        return Inertia::render('ShowLiderIndex', [
            'title' => 'Show de Prêmios Líder'
        ])->withViewData(compact('metaTag'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestShowLider $request)
    {
        //valid nf repetida
       
        try {
            $documento =  str_pad($request->document,9,'0',STR_PAD_LEFT);
            $repeat = ShowLider::where('document',$documento )->where('tenantid',$request->store)->first();

            if(!is_null($repeat)){
                throw new Exception("DOCUMENTO LANÇADO ANTERIORMENTE");
            }

            $show = new ShowLider();
            $show->name = strtoupper($request->name);
            $show->cpf = preg_replace("/[^0-9]/","", $request->cpf);
            $show->cnpj = preg_replace("/[^0-9]/","", $request->cnpj);
            $show->document =$documento;
            $show->tenantid = $request->store;
            $show->phone_number =preg_replace("/[^0-9]/","", $request->phone_number);
            $show->save();
            
            return Redirect()->back()->with(['message'=>'Documento '. $documento . ' cadastrado com sucesso!']);
        } catch (\Throwable $th) {            
            return Redirect()->back()->withErrors(['message'=>$th->getMessage()]);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
