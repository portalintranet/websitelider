<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use App\Models\Product;
use App\Models\ProductGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class ProductGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('ProductGroup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = Product::where('group', $id)->get();
        $title = ProductGroup::where('cod',$id)->first();
        $infoMetaTag =  MetaTag::where('page',strtolower($title->cod))->first();
        $metaTag['title'] =$infoMetaTag->title;
        $metaTag['tags'] = is_null($infoMetaTag)? '': $infoMetaTag->tags;
        $metaTag['description'] = is_null($infoMetaTag)? '': $infoMetaTag->description;
        $metaTag['category'] = is_null($infoMetaTag)? '': $infoMetaTag->category;
        $metaTag['service'] = is_null($infoMetaTag)? '': $infoMetaTag->service;
        $metaTag = (object) $metaTag;

       

        return Inertia::render('ProductGroupShow', [
            'group' => $id,
            'title' => $title->desc,
            'items' => $items, 
            'banner'=> $title->banner1
            ,'aboutProduct' => $title->body
            ])->withViewData(compact('metaTag'));
        }

    public function listProducts(Request $request){
        $query = $request->all();
        $query = $query['query'];
        
        $itens = Product::where('desc','like','%'.$query.'%')->get();
        return $itens;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
