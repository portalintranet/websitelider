<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use App\Models\Product;
use App\Models\ProductDescription;
use App\Models\ProductGroup;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductDescriptionController extends Controller
{



    public function index()
    {
        $productDescriptions = ProductDescription::all(); 
        return view('product-descriptions.index', compact('productDescriptions')); 
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        ProductDescription::create($request->all());

        return redirect()->route('product-descriptions.index')
                         ->with('success', 'Product description created successfully.');
    }

    public function show($cod)
    {
        $items = Product::where('group', $cod)->get(); 
        //$title = ProductGroup::where('cod',$cod)->first();             

        //buscar as informacoes na tabela de meta tags onde o campo "page" é igual ao titulo da pagina corrente
        //strtolower = deixar todas as letras minusculas
        //$infoMetaTag =  [];
        //criar um array (nesse caso metaTag) com title//tags//description// e popular com a informacoes das meta tags
        $metaTag['title'] = 'Produto';
        //is_null = verifica se a busca no banco de dados retornou algo, se nao ele deixa em branco 
        $metaTag['tags'] = 'teste';//aumentar o tamanho desse campo no banco e preencher melhor as infos
        $metaTag['description'] ='teste';
        $metaTag['category'] = 'teste';
        $metaTag['service'] = 'teste';
        //compact é uma funcao para juntar todos os arrays num so 
        
       

        $metaTag = (object) $metaTag;
        return Inertia::render('ProductDescription',
        [
            'product'=> ProductDescription::find($cod),
            'title'=> 'teste 123'
        ])->withViewData(compact('metaTag'));
    }

    public function edit(ProductDescription $productDescription)
    {
        return view('product-descriptions.edit', compact('productDescription'));
    }

    public function update(Request $request, ProductDescription $productDescription)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        $productDescription->update($request->all());

        return redirect()->route('product-descriptions.index')
                         ->with('success', 'Product description updated successfully.');
    }

    public function destroy(ProductDescription $productDescription)
    {
        $productDescription->delete();

        return redirect()->route('product-descriptions.index')
                         ->with('success', 'Product description deleted successfully.');
    }
}
