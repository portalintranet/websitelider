<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestShowLider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cpf' => 'required',
            'cnpj' => 'required',
            'document' => 'required',
            'phone_number' => 'required',
            'store' => 'required',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Campo obrigatório',
        ];
    }  
}
