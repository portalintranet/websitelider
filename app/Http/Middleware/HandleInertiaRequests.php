<?php

namespace App\Http\Middleware;

use App\Models\ProductGroup;
use App\Models\Stores;
use Illuminate\Http\Request;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request): ?string
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request): array
    {
        
        $arrGroups[] = ['id'=>'allproducts','name'=>'Todos os Produtos'];
        $arrGroups[] = ['id'=>'perfis','name'=>'perfis'];
        $arrGroups[] = ['id'=>'placas','name'=>'placas'];
        $arrGroups[] = ['id'=>'isolantes','name'=>'isolantes'];
        $arrGroups[] = ['id'=>'fixadores','name'=>'fixadores'];
        $arrGroups[] = ['id'=>'drywall','name'=>'drywall'];
        $arrGroups[] = ['id'=>'stellframe','name'=>'stell frame'];    
        
        $arrGroups = ProductGroup::all();
        $arrStores = Stores::all();
      
        return array_merge(parent::share($request), [
            'groupProduct' => $arrGroups
            ,'stores' => $arrStores
        ]);
    }
}
